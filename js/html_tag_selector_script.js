/**
 * @file
 * Script functions for module.
 */

(function ($) {
  "use strict";
  Drupal.behaviors.html_tag_selector = {
    attach: function (context, settings) {
      var get_selector_panel = $("#html_tag_selector_select_panel");
      var selectors_list = $("#html_tag_selector_list");
      var activate = $('#html_tag_selector_block_turn_on');
      var dezactivate = $('#html_tag_selector_block_turn_off');
      var reset = $('#html_tag_selector_reset');
      var parent = $('#html_tag_selector_get_parent');
      var next_element = $('#html_tag_selector_get_next');
      var previous_element = $('#html_tag_selector_get_previous');
      var first_child = $('#html_tag_selector_get_first_child');
      var close = $('#html_tag_selector_close');
      var info = $('#html_tag_selector_info');
      var get_selector_button = $('#html_tag_selector_add_selector');
      var copy = $('#html_tag_selector_copy');
      var mouseover_html_tag_selector_block = false;
      var html_tag_selector_on = false;
      var current_element = {};
      var selector_list_counter = 0;

      /**
       * Reset the plugin.
       */
      var html_tag_selector_clean = function () {
        selectors_list.empty();
        info.empty().text("No element!");
        if (current_element.length > 0) {
          current_element.removeClass('html_tag_selector_mouse_over').removeClass('html_tag_selector_get_element');
        }
        selector_list_counter = 0;
      };

      /**
       * When click on the page, we save the object.
       *
       * @param {type} obj object.
       */
      var process_new_tag = function (obj) {
        if (current_element.length > 0) {
          current_element.removeClass('html_tag_selector_mouse_over').removeClass('html_tag_selector_get_element');
        }
        current_element = obj;
        if (current_element.length > 0) {
          current_element.addClass('html_tag_selector_mouse_over');
          info.empty();
          var id = current_element.attr('id');
          var classes = current_element.attr('class');
          var tag = current_element.get(0).tagName;
          info.append("<p>Tag: " + tag + ";</p>");
          if (typeof id !== 'undefined') {
            info.append("<p>Id: " + id + ";</p>");
          }
          if (typeof classes !== 'undefined') {
            info.append("<p>Class: " + classes + ";</p>");
          }
        }
      };

      /**
       * Compare two objects.
       *
       * @param {type} object first object.
       * @param {type} compareTo second obiects.
       *
       * @return {Boolean}
       * If Element are Equal return true;
       */
      var are_equal = function (object, compareTo) {
        if (!compareTo || object.length !== compareTo.length) {
          return false;
        }
        for (var i = 0; i < object.length; ++i) {
          if (object[i] !== compareTo[i]) {
            return false;
          }
        }
        return true;
      };

      /**
       * Get the selector of a object.
       *
       * @param {type} obj object.
       *
       * @return {undefined} the selector.
       */
      var get_selector = function (obj) {
        var selector = "";
        var pozition;
        var valid_selector = true;
        var tag;

        while (valid_selector && $(obj).length > 0) {

          pozition = (parseFloat($(obj).index()) + 1).toString();
          var id = $(obj).attr('id');
          var cl = $(obj).removeClass('html_tag_selector_mouse_over').attr('class');
          if (cl !== '' && typeof cl !== 'undefined') {
            var classes = cl.split(" ");
            cl = classes[0];
          }
          tag = $(obj).get(0).tagName;
          if (tag === 'BODY' || tag === 'HTML') {
            selector = tag + selector;
            valid_selector = false;
          }
          else {
            if (cl !== '' && $(tag + '.' + cl).length === 1) {
              selector = tag + '.' + cl + (selector !== '' ? ' >' : '') + selector;
              valid_selector = false;
            }
            else {
              if (id !== '' && $(tag + '#' + id).length === 1) {
                selector = tag + '#' + id + (selector !== '' ? ' >' : '') + selector;
                valid_selector = false;
              }
              else {
                selector = ' ' + tag + ':nth-child(' + pozition + ') ' + selector;
              }
            }
          }
          obj = $(obj).parent();
        }
        return selector;
      };

      /**
       * Check if selector has not already exist on list, and adding selector.
       */
      var add_new_selector = function () {
        var selector = get_selector(current_element);
        var row = $("<li></li>");
        row.attr('title', 'Remove');
        row.text(selector);
        // See the object.
        row.mouseover(function () {
          $($(this).text()).addClass('html_tag_selector_get_element');
        });
        row.mouseout(function () {
          $($(this).text()).removeClass('html_tag_selector_get_element');
        });
        row.click(function () {
          $($(this).text()).removeClass('html_tag_selector_get_element');
          $(this).remove();
        });
        var valid = true;
        selectors_list.find("li").each(function () {
          if (are_equal($($(this).text()), $(selector))) {
            valid = false;
          }
        });
        if (valid) {
          row.attr('row_nr', ++selector_list_counter);
          selectors_list.append(row);
        }
        else {
          alert("Selector Already Exist!");
        }
      };

      /*
       * Reset the plugin.
       */
      reset.click(function () {
        html_tag_selector_clean();
      });

      /*
       * Turn of and on buttons.
       */
      activate.click(function () {
        $(this).hide();
        dezactivate.show();
        get_selector_panel.show();
        html_tag_selector_clean();
        html_tag_selector_on = true;
      });
      dezactivate.click(function () {
        $(this).hide();
        activate.show();
        get_selector_panel.hide();
        html_tag_selector_on = false;
      });
      close.click(function () {
        dezactivate.click();
      });

      /*
       * Desable plugin when we click on the plugin panel.
       */
      $('#html_tag_selector_block_div').mouseover(function () {
        mouseover_html_tag_selector_block = true;
      });

      /*
       * Enable plugin when we click on the plugin panel.
       */
      $('#html_tag_selector_block_div').mouseout(function () {
        mouseover_html_tag_selector_block = false;
      });

      /*
       * Getting element when we click on the element on page.
       */
      $('*').click(function (event) {
        if (!mouseover_html_tag_selector_block && html_tag_selector_on) {
          process_new_tag($(this));
          event.stopPropagation();
          return false;
        }
      });

      /*
       * Go To neighbours.
       */
      parent.click(function () {
        if ($(current_element).parent().length > 0) {
          process_new_tag($(current_element).parent());
        }
        else {
          alert('No Parents!');
        }
      });
      next_element.click(function () {
        if ($(current_element).next().length > 0) {
          process_new_tag($(current_element).next());
        }
        else {
          alert('No Next Element!');
        }
      });
      previous_element.click(function () {
        if ($(current_element).prev().length > 0) {
          process_new_tag($(current_element).prev());
        }
        else {
          alert('No Previous Element!');
        }
      });
      first_child.click(function () {
        if ($(current_element).find(">:first-child").length > 0) {
          process_new_tag($(current_element).find(">:first-child"));
        }
        else {
          alert('No Children!');
        }
      });

      /*
       * Click on get Selector.
       */
      get_selector_button.click(function () {
        if (current_element.length > 0) {
          add_new_selector();
        }
        else {
          alert("There is no Element!");
        }
      });

      /*
       * Get Selectors.
       */
      copy.click(function () {
        if (selectors_list.find('li').length > 0) {
          var selectors = '';
          var rows = selectors_list.find('li');
          var i = 0;
          rows.each(function () {
            ++i;
            selectors += $(this).text() + (i < rows.length ? ', ' : '');
          });
          window.prompt("Copy to clipboard: Ctrl+C, Enter", selectors);
        }
        else {
          alert("No elements!!!");
        }
      });

      $('#html_tag_selector_move').click(function () {
        if ($(this).text() === '>') {
          $(this).parent().css({right: '0px', left: 'initial'});
          $(this).text('<');
        }
        else {
          $(this).parent().css({left: '0px', right: 'initial'});
          $(this).text('>');
        }
      });
    }
  };
}(jQuery));
