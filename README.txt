* Introduction
This is a module for getting the selector of the html tag of a web page by
clicking on the page.

* Requirements
This module does not have any dependencies.

* Installation
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

* Configuration
Enable the HTML TAG Selector Block from: Structure -> Blocks

*HowToUse
Click on Activate button from the Html Tag Selector Block. After that click on
the element from web page you want to get the selector. You can use the buttons
from panel to go to the parent of the element, to next element, to previous 
element, to first child. After you get get your element from Html page, click 
on Add selector to list in order to get the selector of the element. You can
get as many selectors as you want. In order to copy the selector click on the
Click to Clipboard button and you will get a string with all your selectors
delimited with a comma. In order to move the panel from the left side click on 
the arrow from the top-left of the panel. In order to close the panel click on
the Close button from panel or on the Deactivate button from Html Tag Selector
block. If you want to reset the list of selectors click on reset button from
the panel. If you want to delete a selector click on it. Also if you will put
the cursor on the selector from the panel, the element on the Web page will
have red outline.

* Maintainers
Current maintainers:
Mosnoi Ion - https://drupal.org/user/2295214
