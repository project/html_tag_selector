<?php
/**
 * @file
 * Main div html_tag_selector_block.
 */
?>
<div id="html_tag_selector_block_div">
  <div id="html_tag_selector_control_buttons">
    <input id="html_tag_selector_block_turn_on" type="button" value="<?php print t("Activate"); ?>" />
    <input id="html_tag_selector_block_turn_off" type="button" value="<?php print t("Deactivate");?>" />
  </div>
  <div id="html_tag_selector_select_panel">
    <span id="html_tag_selector_move">></span>
    <div id='html_tag_selector_object_select'>
      <div id="html_tag_selector_info">
        <p><?php print t("No Element!!!");?></p>
      </div>
      <table>
        <tr>
          <td>
            <div><?php print t('Parent');?></div>
          </td>
          <td>
            <input id='html_tag_selector_get_parent' type='button' value="<?php print t('Get Parent');?>" />
          </td>
        </tr>
        <tr>
          <td>
            <div><?php print t('Element');?></div>
          </td>
          <td>
            <input id='html_tag_selector_get_next' type='button' value="<?php print t('Next');?>" />
            <input id='html_tag_selector_get_previous' type='button' value="<?php print t('Previous');?>" />
          </td>
        </tr>
        <tr>
          <td>
            <div><?php print t('Children');?></div>
          </td>
          <td>
            <input id='html_tag_selector_get_first_child' type='button' value="<?php print t('First Child');?>" />
          </td>
        </tr>
      </table>
    </div>  
    <div class='html_tag_selector_buttons'>
      <input id='html_tag_selector_add_selector' type='button' value="<?php print t('Add Selector to list');?>" />
    </div>  
    <div id="html_tag_selector_list_div">
      <ul id='html_tag_selector_list'></ul>
    </div>
    <div class='html_tag_selector_buttons'>
      <input id='html_tag_selector_copy' type='button' value="<?php print t('Copy to Clipboard');?>" />
      <input id='html_tag_selector_reset' type='button' value="<?php print t('Reset');?>" />
      <input id='html_tag_selector_close' type='button' value="<?php print t('Close');?>" />
    </div>  
  </div>  
</div>
